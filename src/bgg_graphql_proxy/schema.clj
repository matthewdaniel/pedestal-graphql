(ns bgg-graphql-proxy.schema
  (:require
    [clojure.java.io :as io]
    [clojure.edn :as edn]
    [bgg-graphql-proxy.db :as db]
    [com.walmartlabs.lacinia.schema :as schema]
    [com.walmartlabs.lacinia.util :refer [attach-resolvers attach-scalar-transformers]]
    [bgg-graphql-proxy.client :as client]))

(defn ^:private resolve-board-game
  [context args _value]
  ;; TODO: Error handling, including not found
  (client/get-board-game (:cache context) (:id args)))

(defn ^:private resolve-search
  [context args _value]
  (client/search (:cache context) (:term args)))

(defn ^:private extract-ids
  [board-game key args]
  (let [{:keys [limit]} args]
    (cond->> (get board-game key)
      limit (take limit))))

(defn ^:private resolve-game-publishers
  [context args board-game]
  (client/publishers (:cache context) (extract-ids board-game :publisher-ids args)))

(defn ^:private resolve-game-designers
  [context args board-game]
  (client/designers (:cache context) (extract-ids board-game :designer-ids args)))
(defn ^:private print-that-shit [data] (pprint data) data)
; (defn bgg-schema
;   []
;   (-> (io/resource "bgg-schema.edn")
;       slurp
;       edn/read-string
;       (attach-resolvers {:resolve-game resolve-board-game
;                          :resolve-search resolve-search
;                          :resolve-game-publishers resolve-game-publishers
;                          :resolve-game-designers resolve-game-designers})
;       schema/compile))

(defn bgg-schema
  []
  (-> (io/resource "bgg-schema.edn")
      slurp
      edn/read-string
      (attach-scalar-transformers
        {:scalar-date-parser (fn [v] "testing")
         :scalar-date-serializer (schema/as-conformer (fn [v] (println v) "test2"))
         :scalar-hour-minute-parser (fn [v] "test")
         :scalar-true-serializer (fn [v] "test")
         :scalar-true-parser (fn [v] "test")
         :scalar-hour-minute-serializer (fn [v] "testing")})
      (attach-resolvers
        {:resolve-devices db/resolve-devices
         :resolve-device-person db/resolve-device-person
         :resolve-device-pings db/resolve-device-pings
         :today-resolver (fn [ctx args v] "test")})
      ; attach-enum-scalars
      ; detatch-enums
      schema/compile))
