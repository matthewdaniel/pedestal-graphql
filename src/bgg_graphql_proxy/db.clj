(ns bgg-graphql-proxy.db
  (:require [clojure.data.json :as json]))


(defn example-device []
 {:id (rand-int 30)
  :identifier (+ (rand-int 1000000) 10000)
  :voltage (/ (rand-int 100) 100)
  :fuel (rand-int 100)
  :created "test"

  :name "Some Device Name"
  :notes "A bunch of notes"})

(defn example-person[]
 {:id (rand-int 30)
  :firstName "Ben"
  :lastName "Smith"
  :activeDevices (take 5 (repeatedly example-device))})


(defn resolve-device-pings [] [])

(defn resolve-devices
  [context args _value]
  (take 5 (repeatedly example-device)))

(defn resolve-device-person
  [context args _value]
  {:id 1
   :firstName "Ben"
   :lastName "Smith"
   :image "http://something.com"})

(defn resolve-person-device
  [context args _value]
  (take 5 (repeatedly example-device)))
